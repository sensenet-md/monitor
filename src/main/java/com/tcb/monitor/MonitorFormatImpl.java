package com.tcb.monitor;

import java.util.Arrays;
import java.util.List;

public class MonitorFormatImpl implements MonitorFormat {
	private static List<String> header = Arrays.asList("Name","Calls","Time per Call","Accumulated Time");
	private static String headerFormat = "%12s %12s %12s %12s";
	private static String format = "%20s %20d %20.0f %20d";
	private String name;
	
	public MonitorFormatImpl(String name){
		this.name = name;
	}
	
	@Override
	public String toString(){
		Long accumulatedTime = MonitorStorage.accumulatedTime.get(name);
		Long calls = MonitorStorage.calls.get(name);
		Double timePerCall = ((double) accumulatedTime) / ((double) calls);
		return String.format(format, name,calls,timePerCall,accumulatedTime);
	}
	
	public static String getHeader(){
		return String.format(headerFormat,header.get(0),header.get(1),header.get(2),header.get(3));
		
	}
}
