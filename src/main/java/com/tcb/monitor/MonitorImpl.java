package com.tcb.monitor;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoField;
import java.util.Optional;

public class MonitorImpl implements Monitor {
	
	private String name;
	private Optional<Instant> startTime;
	private Optional<Instant> stopTime;

	public MonitorImpl(String name){
		this.name = name;
		this.startTime = Optional.empty();
		this.stopTime = Optional.empty();
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void start() {
		startTime = Optional.of(Instant.now());
	}
	
	private Instant getStartTime(){
		return startTime.orElseThrow(() -> new RuntimeException("Monitor was not started yet"));
	}
	
	private Instant getStopTime(){
		return stopTime.orElseThrow(() -> new RuntimeException("Monitor was not stopped yet"));
	}

	@Override
	public void stop() {
		stopTime = Optional.of(Instant.now());
		MonitorStorage.accumulatedTime.putIfAbsent(name, 0l);
		MonitorStorage.calls.putIfAbsent(name,0l);
	    MonitorStorage.accumulatedTime.compute(name, (name,oldTime) -> oldTime + getTime());
	    MonitorStorage.calls.compute(name, (name,oldCalls) -> oldCalls + 1);
	}
	
	@Override
	public Long getTime() {
		Long time = Duration.between(getStartTime(), getStopTime()).toMillis();
		return time;
	}

	@Override
	public Long getAccumulatedTime() {
		return MonitorStorage.accumulatedTime.getOrDefault(name, 0l);
	}

}
