package com.tcb.monitor;

public interface Monitor{
	public String getName();
	public void start();
	public void stop();
	public Long getTime();
	public Long getAccumulatedTime();
}
