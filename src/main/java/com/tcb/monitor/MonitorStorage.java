package com.tcb.monitor;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

public class MonitorStorage {
	public static final ConcurrentHashMap<String,Long> accumulatedTime = new ConcurrentHashMap<String,Long>();
	public static final ConcurrentHashMap<String,Long> calls = new ConcurrentHashMap<String,Long>();
	
	public static void save(String fileName) throws FileNotFoundException, UnsupportedEncodingException{
		Enumeration<String> keys = calls.keys();
		PrintWriter writer = new PrintWriter(fileName, "UTF-8");
		writer.write(getString());
		writer.close();
		reset();
	}
	
	public static String getString(){
		String out = MonitorFormatImpl.getHeader() + "\n";
		Enumeration<String> keys = calls.keys();
		while(keys.hasMoreElements()){
			String key = keys.nextElement();
			MonitorFormat format = new MonitorFormatImpl(key);
			out += format.toString() + "\n";
		}
		return out;
	}
	
	public static void reset(){
		accumulatedTime.clear();
		calls.clear();
	}
}
